package com.sourceit.l2task3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText first_input_window;
    EditText second_input_window;
    EditText third_input_window;
    EditText first_show_window;
    EditText second_show_window;
    EditText third_show_window;
    Button binary;
    Button octal;
    Button hexadecimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        first_input_window = (EditText) findViewById(R.id.first_input_window);

        second_input_window = (EditText) findViewById(R.id.second_input_window);

        third_input_window = (EditText) findViewById(R.id.third_input_window);

        first_show_window = (EditText) findViewById(R.id.first_show_window);

        second_show_window = (EditText) findViewById(R.id.second_show_window);

        third_show_window = (EditText) findViewById(R.id.third_show_window);

        binary = (Button) findViewById(R.id.binary);
        binary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!first_input_window.getText().toString().isEmpty()) {
                    first_show_window.setText(Integer.toBinaryString(Integer.parseInt(first_input_window.getText().toString())));
                }else {
                    first_show_window.setText("Must be only numbers");
                }
            }
        });

        octal = (Button) findViewById(R.id.octal);
        octal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!second_input_window.getText().toString().isEmpty()) {
                    second_show_window.setText(Integer.toOctalString(Integer.parseInt(second_input_window.getText().toString())));
                }else {
                    first_show_window.setText("Must be only numbers");
                }
            }
        });

        hexadecimal = (Button) findViewById(R.id.hexadecimal);
        hexadecimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!third_input_window.getText().toString().isEmpty()) {
                    third_show_window.setText(Integer.toHexString(Integer.parseInt(third_input_window.getText().toString())));
                }else {
                    third_show_window.setText("Must be only numbers");
                }
            }
        });

    }
}
